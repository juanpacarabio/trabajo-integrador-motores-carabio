using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push : MonoBehaviour
{
    public Camera camara;
    public AudioSource empujar;
    public Rigidbody objeto;
    public float fuerza;
    public void Impulso()
    {
        empujar.Play();
        objeto.AddForce(camara.transform.forward * fuerza);
    }
}
