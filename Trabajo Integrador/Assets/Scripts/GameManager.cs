using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject girado;
    public GameObject jugador;
    public GameObject heavy;
    public GameObject detectaCañones;
    void Start()
    {

    }

    public static bool timonel = false;
    public static float tiempo;
    public static bool heavycompleto = false;
    public static bool fin = false;
    public static bool cañones = false;
    private int cadenas = 0;

    void Update()
    {
        timonel = girado.GetComponent<RotarTimonel>().estagirando;
        tiempo = jugador.GetComponent<ControlJugador>().tiempoRestante;
        fin = jugador.GetComponent<ControlJugador>().resume;
        heavycompleto = heavy.GetComponent<DetectHeavy>().balance;
        cañones = detectaCañones.GetComponent<DetectarCañones>().completo;
        cadenas = ControlJugador.cadenas_rotas;

        if ((tiempo <= 0) && (timonel == true) && (heavycompleto == true) && (cañones == true) && (cadenas == 3))
        {
            Victoria();
        } else if (tiempo <= 0)
        {
            DerrotaTiempo();
        }

        if (fin == true)
        {
            if (timonel == false)
            {
                DerrotaTimonel();
                Debug.Log("Habria que girar el timonel");
            } else if (heavycompleto == false)
            {
                DerrotaHeavy();
                Debug.Log("Habria que mover el cargamento al lado opuesto");
            } else if (cañones == false)
            {
                DerrotaCañones();
                Debug.Log("Habria que tirar los cañones de la derecha");
            } else if ((heavycompleto == true) && (timonel == true) && (cañones == true))
            {
                Victoria();
            }else if (cadenas < 3)
            {
                DerrotaCadenas();
            }
        }
    }

    void Victoria()
    {
        SceneManager.LoadScene("Victoria");
        Time.timeScale = 1;
    }

    void DerrotaTimonel()   //Requisito: El timonel no esta girando
    {
        SceneManager.LoadScene("DerrotaTimonel");
        Time.timeScale = 1;
    }

    void DerrotaHeavy()     //Requisito: Menos de X cantidad de objetos "Heavy" en el lado dercho del barco A && 0 cañones del lado derecho del barco A
    {
        SceneManager.LoadScene("DerrotaHeavy");
        Time.timeScale = 1;
    }

    void DerrotaTiempo()    //Requisito: Se acaba el tiempo
    {
        SceneManager.LoadScene("DerrotaTiempo");
        Time.timeScale = 1;
    }

    void DerrotaCañones()   //Requisito: Los cañones del lado derecho siguen ahi
    {
        SceneManager.LoadScene("DerrotaCañones");
        Time.timeScale = 1;
    }

    void DerrotaCadenas()
    {
        SceneManager.LoadScene("DerrotaCadenas");
        Time.timeScale = 1;
    }

    
}
