using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectarCañones : MonoBehaviour
{
    public bool completo = false;
    private int cantidad = 0;
    public int cantidadNecesaria = 4;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Push")
        {
            cantidad++;
            Debug.Log("Ha Caido un cañon");
        }

        if (cantidad >= cantidadNecesaria)
        {
            completo = true;
            Debug.Log("Se alcanzo el balance");
        }
    }
}
