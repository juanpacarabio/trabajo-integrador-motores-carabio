using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectHeavy : MonoBehaviour
{
    public bool balance = false;
    private int cantidad = 0;
    public int cantidadNecesaria;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Heavy")
        {
            cantidad++;
            Debug.Log("Nuevo Heavy Ingresado");
        }

        if (cantidad >= cantidadNecesaria)
        {
            balance = true;
            Debug.Log("Se alcanzo el balance");
        }
    }
}
