using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgarrarEsfera : MonoBehaviour
{
    public Transform PuntoAgarre;
    public AudioSource agarrar;
    public GameObject jugador;
    bool BrazoAgarre;
    private void OnMouseDown()
    {
        BrazoAgarre = jugador.GetComponent<ControlJugador>().brazos[0].activeInHierarchy;

        if (BrazoAgarre == true)
        {
            GetComponent<SphereCollider>().enabled = false;
            GetComponent<Rigidbody>().useGravity = false;
            this.transform.position = PuntoAgarre.position;
            this.transform.parent = GameObject.Find("PuntoAgarre").transform;
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            agarrar.Play();
        }
    }

    private void OnMouseUp()
    {
        this.transform.parent = null;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<SphereCollider>().enabled = true;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
    }
}
