using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarTimonel : MonoBehaviour
{
   public bool estagirando = false;
    private void Update()
    {
        if(estagirando == true)
        {
            Girar();
        }
    }
    public void Girar()
    {
        transform.Rotate(new Vector3(0, 0, 50) * Time.deltaTime);
        estagirando = true;
    }
}
