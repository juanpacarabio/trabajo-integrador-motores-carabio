using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RomperCadena : MonoBehaviour
{
    
    private int golpes_para_romper;
    void Start()
    {
        golpes_para_romper = 3;
    }

    
    void Update()
    {
        
    }

    public void RecibirGolpe()
    {
        Debug.Log("Cadena recibio da�o");
        golpes_para_romper--;

        if (golpes_para_romper <= 0)
        {
            Destruir();
        }

    }

    public void Destruir()
    {
        Destroy(gameObject);
        ControlJugador.cadenas_rotas++;
    }

   
}
