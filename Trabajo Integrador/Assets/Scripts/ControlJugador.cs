﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    [Header("Control Jugador")]
    public float rapidezDesplazamiento = 10.0f;
    public float rapidezSprint = 17.0f;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    bool estaAgachado = false;
    public GameObject jugador;
    public Vector3 tamañoNormal = new Vector3(1.7f, 1.7f, 1.7f);
    public Vector3 tamañoAgachado = new Vector3(1.0f, 1.0f, 1.0f);
    public Camera camara;
    public bool resume = false;
    public GameObject[] brazos;
    public GameObject[] linternas;
    bool luzprendida = true;
    public GameObject martillo_recostado;
    bool tieneMartillo;
    public static int cadenas_rotas = 0;
    int ultimo_brazo;
    bool camara_lenta = false;
    float tiempo_lento;


    [Header("Cosas UI")]
    public GameObject[] elementos_ui;
    public GameObject ui_pausa;
    public GameObject ui_herramientas;
    public TMPro.TMP_Text mano_libre;
    public TMPro.TMP_Text objetivo1;
    public TMPro.TMP_Text objetivo2;
    public TMPro.TMP_Text objetivo3;
    public TMPro.TMP_Text objetivo4;
    public TMPro.TMP_Text textoTimer;
    public GameObject ui;
    public static bool pausa = false;
    bool cañones;
    bool timonel;
    bool cargo;

    void Start()
    {
        StartCoroutine(ComenzarCronometro(300));
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
    }
    void Update()
    {
        cañones = GameManager.cañones;
        timonel = GameManager.timonel;
        cargo = GameManager.heavycompleto;

        if (brazos[0].activeInHierarchy)
        {
            ultimo_brazo = 0;
        } else if (brazos[1].activeInHierarchy)
        {
            ultimo_brazo = 1;
        } else if (brazos[2].activeInHierarchy)
        {
            ultimo_brazo = 2;
        } else if (brazos[3].activeInHierarchy)
        {
            ultimo_brazo = 3;
        }

        if (timonel == true)
        {
            objetivo4.text = " ̶-̶ ̶T̶u̶r̶n̶ ̶t̶h̶e̶ ̶w̶h̶e̶e̶l̶!̶";
        }
        if (cañones == true)
        {
            objetivo2.text = " ̶-̶ ̶T̶h̶r̶o̶w̶ ̶t̶h̶e̶ ̶e̶x̶t̶r̶a̶ ̶w̶e̶i̶g̶h̶t̶ ̶o̶v̶e̶r̶b̶o̶a̶r̶d̶!̶";
        }
        if (cargo == true)
        {
            objetivo1.text = " ̶-̶ ̶S̶h̶i̶f̶t̶ ̶s̶o̶m̶e̶ ̶w̶e̶i̶g̶h̶t̶ ̶t̶o̶ ̶t̶h̶e̶ ̶l̶e̶f̶t̶!̶";
        }
        if (cadenas_rotas >= 3)
        {
            objetivo3.text = " ̶-̶ ̶B̶r̶e̶a̶k̶ ̶t̶h̶e̶ ̶a̶n̶c̶h̶o̶r̶s̶!̶";
        }


        if (tiempoRestante <= 270)
        {
            if (cargo == false)
            {
                objetivo1.text = "- Shift some weight to the left! About 7 pieces of cargo should do it!";
            }
        }
        if (tiempoRestante <= 230)
        {
            if (cañones == false)
            {
                objetivo2.text = "- Throw the extra weight overboard! The cannons on the right will do!";
            }
        }
        if (tiempoRestante <= 200)
        {
            if (cadenas_rotas < 3)
            {
                objetivo3.text = "- Break the anchors! I swear I saw a hammer around here...";
            }
        }
        if (tiempoRestante <= 100)
        {
            if (timonel == false)
            {
                objetivo4.text = "- Turn the wheel! I don´t believe you need help for that.";
            }
        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (tieneMartillo == true)
        {
            mano_libre.text = "Hammer";
        }

        if (Input.GetKey(KeyCode.Tab))
        {
            if (pausa == false)
            {
                Time.timeScale = 0.05f;
                elementos_ui[0].SetActive(false);
                elementos_ui[1].SetActive(false);
                ui_herramientas.SetActive(true);

                brazos[ultimo_brazo].SetActive(false);
            }
        } else if (Input.GetKeyUp(KeyCode.Tab))
        {
            Time.timeScale = 1;
            elementos_ui[0].SetActive(true);
            elementos_ui[1].SetActive(true);
            ui_herramientas.SetActive(false);

            brazos[ultimo_brazo].SetActive(true);
        }

        Time.timeScale += 0.2f * Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
        if (Time.timeScale == 1f)
        {
            camara_lenta = false;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (camara_lenta == false)
            {
                CamaraLenta();
                camara_lenta = true;
            }else
            {
                Time.timeScale = 1f;
                camara_lenta = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.LeftControl) && (estaAgachado == false))
        {
            jugador.transform.localScale = tamañoAgachado;
            estaAgachado = true;
        }
        else if (Input.GetKeyDown(KeyCode.LeftControl) && (estaAgachado == true))
        {
            jugador.transform.localScale = tamañoNormal;
            estaAgachado = false;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                if (hit.collider.name == "Timonel1")
                {
                    Debug.Log("El Rayo toco el Objeto" + hit.collider.name);
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);
                    RotarTimonel scriptObjetoTocado = (RotarTimonel)objetoTocado.GetComponent(typeof(RotarTimonel));

                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.Girar();
                    }

                }
                else if (hit.collider.tag == "Push")
                {
                    if (brazos[2].activeInHierarchy == true)
                    {
                        GameObject objetoTocado = GameObject.Find(hit.transform.name);
                        Push scriptObjetoTocado = (Push)objetoTocado.GetComponent(typeof(Push));


                        if (scriptObjetoTocado != null)
                        {
                            scriptObjetoTocado.Impulso();
                        }
                    }
                } else if (hit.collider.name == "Martillo")
                {
                    martillo_recostado.SetActive(false);
                    tieneMartillo = true;
                }

            }
        }
        else if (Input.GetKeyDown(KeyCode.T))
        {
            resume = true;
        } else if (Input.GetKeyDown("escape"))
        {

            if (pausa == false)
            {
                brazos[0].SetActive(false);
                brazos[2].SetActive(false);
                brazos[1].SetActive(false);
                brazos[3].SetActive(false);
                elementos_ui[0].SetActive(false);
                ui.SetActive(false);
                brazos[4].SetActive(true);
                ui_pausa.SetActive(true);



                Time.timeScale = 0;
                pausa = true;

            } else if (pausa == true)
            {
                brazos[4].SetActive(false);
                ui_pausa.SetActive(false);
                elementos_ui[0].SetActive(true);
                brazos[ultimo_brazo].SetActive(true);
                if (brazos[3].activeInHierarchy)
                {
                }else
                {
                    elementos_ui[1].SetActive(true);
                }

                Time.timeScale = 1;
                pausa = false;
            }

            if (Cursor.visible == true)
            {

                Cursor.visible = false;

            } else if (Cursor.visible == false)
            {
                Cursor.visible = true;
            }

        } else if ((jugador.transform.position.y <= -2) || Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("NivelBarco");
            Time.timeScale = 1;
        } else if (Input.GetKeyDown(KeyCode.Alpha1))
        {

            brazos[1].SetActive(false);
            brazos[2].SetActive(false);
            brazos[3].SetActive(false);
            brazos[0].SetActive(true);
            ui.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {

            brazos[0].SetActive(false);
            brazos[2].SetActive(false);
            brazos[3].SetActive(false);
            brazos[1].SetActive(true);
            ui.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {

            brazos[0].SetActive(false);
            brazos[1].SetActive(false);
            brazos[3].SetActive(false);
            brazos[2].SetActive(true);
            ui.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (tieneMartillo == false)
            {
                brazos[0].SetActive(false);
                brazos[2].SetActive(false);
                brazos[1].SetActive(false);
                ui.SetActive(false);

            } else if (tieneMartillo == true)
            {
                brazos[0].SetActive(false);
                brazos[2].SetActive(false);
                brazos[1].SetActive(false);
                brazos[3].SetActive(true);
                ui.SetActive(false);

            }

        } else if (Input.GetMouseButtonDown(0))
        {
            if (brazos[3].activeInHierarchy == true)
            {

                Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;

                if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
                {
                    if (hit.collider.tag == "Ancla")
                    {
                        Debug.Log("Le pegaste al Ancla");
                        GameObject objeto_golpeado = GameObject.Find(hit.transform.name);
                        RomperCadena golpear = (RomperCadena)objeto_golpeado.GetComponent(typeof(RomperCadena));
                        if (golpear != null)
                        {
                            golpear.RecibirGolpe();
                        }
                    }
                }
            }
            if (Cursor.lockState == CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        } else if (Input.GetKeyDown("f"))
        {
            if (luzprendida == true)
            {
                linternas[0].SetActive(false);
                linternas[1].SetActive(false);
                linternas[2].SetActive(false);
                linternas[3].SetActive(false);
                linternas[4].SetActive(false);
                linternas[5].SetActive(false);
                luzprendida = false;
            } else if (luzprendida == false)
            {
                linternas[0].SetActive(true);
                linternas[1].SetActive(true);
                linternas[2].SetActive(true);
                linternas[3].SetActive(true);
                linternas[4].SetActive(true);
                linternas[5].SetActive(true);
                luzprendida = true;
            }
        }
    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    public float tiempoRestante;
    public float pasoTiempo = 1.0f;
    public IEnumerator ComenzarCronometro(float valorCronometro = 300)
    {

        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            textoTimer.text = "Stasis Time Left:" + tiempoRestante + "s";
            yield return new WaitForSeconds(pasoTiempo);
            tiempoRestante--;
        }
    }

    void CamaraLenta()
    {
        Time.timeScale = 0.05f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        tiempoRestante = tiempoRestante - 30;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp") == true)
        {
            pasoTiempo = pasoTiempo * 2;
            other.gameObject.SetActive(false);
        }
    }
}
