using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarrar : MonoBehaviour
{
    public Transform PuntoAgarre;
    public AudioSource agarrar;
    public GameObject jugador;
    bool BrazoAgarre;
    private void OnMouseDown()
    {
        BrazoAgarre = jugador.GetComponent<ControlJugador>().brazos[0].activeInHierarchy;

        if (BrazoAgarre == true)
        {

            GetComponent<BoxCollider>().enabled = false;
            GetComponent<Rigidbody>().useGravity = false;
            this.transform.position = PuntoAgarre.position;
            this.transform.parent = GameObject.Find("PuntoAgarre").transform;
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            agarrar.Play();
        }
    }

    private void OnMouseUp()
    {
        this.transform.parent = null;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<BoxCollider>().enabled = true;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
    }
}
