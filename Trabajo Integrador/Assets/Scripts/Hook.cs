using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour
{
    private LineRenderer lr;
    private Vector3 puntohook;
    public LayerMask hookeable;
    public Transform SalidaGrapple, camara, jugador;
    public float distanciaMax = 10f;
    private SpringJoint union;
    public AudioSource sonidofallo;
    public AudioSource sonidoexito;
    public GameObject origen;
    bool BrazoGrapple;


    private void Awake()
    {
        lr = GetComponent<LineRenderer>();

    }

    private void Update()
    {
        BrazoGrapple = origen.GetComponent<ControlJugador>().brazos[1].activeInHierarchy;
        if (BrazoGrapple == true)
        {
            if (Input.GetMouseButtonDown(1))
            {
                UsarHook();


            }
            else if (Input.GetMouseButtonUp(1))
            {
                SoltarHook();
            }
        }
    }

    private void LateUpdate()
    {
        HacerLinea();
    }

    public float spring = 4.5f;
    public float damper = 7f;
    public float massScale = 4.5f;
    void UsarHook()
    {
        RaycastHit hit;
        if (Physics.Raycast(camara.position, direction: camara.forward, out hit, distanciaMax, hookeable))
        {
            sonidoexito.Play();
            puntohook = hit.point;
            union = jugador.gameObject.AddComponent<SpringJoint>();
            union.autoConfigureConnectedAnchor = false;
            union.connectedAnchor = puntohook;

            float distancia = Vector3.Distance(a: jugador.position, b: puntohook);
            union.maxDistance = distancia * 0.8f;
            union.minDistance = distancia * 0.25f;

            union.spring = spring;
            union.damper = damper;
            union.massScale = massScale;

            lr.positionCount = 2;
        }else
        {
            sonidofallo.Play();
        }
        
    }

    void HacerLinea()
    {
        if (!union) return;
        lr.SetPosition(index: 0, SalidaGrapple.position);
        lr.SetPosition(index: 1, puntohook);
    }

    void SoltarHook()
    {
        lr.positionCount = 0;
        Destroy(union);
    }
}
